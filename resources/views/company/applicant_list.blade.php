@php
$i=0;
$jumlahData = 3;
@endphp

@foreach ($applicant as $applicants)
@php
if ($i++ % $jumlahData == 0) {
    echo "<div class='row margin-bottom-10'>";
}
@endphp

<div class="col-xs-12 col-sm-3 col-md-4 col-lg-4">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-10">
                    <p><b>{{$applicants->name}}</b></p>
                </div>
                <div class="col-sm-2">
                    <i class="far fa-star"></i>
                </div>
            </div>
        </div>
    <img class="card-img-top" src="{{asset('storage/app/public/photo_applicant/'.$applicants->photo)}}" alt="Card image top" width="200" height="200">
        <div class="card-body">
            <h4 class="card-title">{{$applicants->level_name}} {{$applicants->majors_name}}</h4>
            
            <p class="card-text">{{$applicants->kota}}</p>
        </div>
    </div>
</div>
@php
if ($i % $jumlahData == 0 || $i == count($applicant)) {
    echo "</div>";
}
@endphp

@endforeach